# Request management helper
This is a simple helper to address the problem of composing and tracking
messages for a request tracker system like (the one part of) TOPdesk.
With this tool messages are to be sanely and locally
1. composed with an editor; and
2. version-controlled.

The overall goal is to make using badly designed issue trackers more
bearable.
